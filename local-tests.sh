#!/usr/bin/env bats

@test "Check for available installation." {
    command -v ./going
}

@test "Verify GET requests." {
    run bash -c "./going to-get http://imgur.com > testfiles/test-get; \
    grep Fastly-Debug-Digest testfiles/test-get"
}

@test "Check for file download." {
    run bash -c "./going to-get file -u http://ftp.gnu.org/gnu/bash/bash-4.3.30.tar.gz -f testfiles/bash-4.3.30.tar.gz"
    [ -e testfiles/bash-4.3.30.tar.gz ]
}

@test "Verify headers are pulled." {
    run bash -c "./going to-get headers https://imgur.com > testfiles/test-headers; \
    grep Fastly-Debug-Digest test-headers"
}

@test "Verify redirect." {
     run bash -c "./going to-get redirection http://bit.ly/1dNVPAW > testfiles/test-redirect; \
    grep -E \"google\" testfiles/test-redirect"
}
