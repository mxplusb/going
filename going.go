package main

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/cheggaaa/pb"
	"github.com/codegangsta/cli"
	"io"
	"net/http"
	"os"
)

/*
Here are the help descriptions.
 */

var toGetHelp = `This command is for HTTP GET requests.
Any and all GET requests are processed through "going to-get".
By default, "going to-get" is just that, a GET request. No
processing is done to the request, no object manipulation
of any kind.

To handle HTTP headers, utilise "going to-get headers".
This is not the same as "going to-get" as it makes a HEAD
- as opposed to GET - request and nicely formats the
output. The reason it's "to-get headers" over "to-headers"
is due to the primary desire for a more natural language
feel when utilising this utility.

Downloading a file is simple, "going to-get file". Individual
filenames are required, currently going does not parse the
URL for a filename.

Sometimes URL redirection is frustrating, especially when
permalinking various HTTP objects. To see the final URL
within a redirected URL, "going to-get redirection" is the
way you want to go.`

func main() {

	application := cli.NewApp()
	application.Name = "going"
	application.Usage = "http utility for various http tasks"
	application.Version = "v0.1.3"
	application.Author = "Mike Lloyd"
	application.Email = "kevin.michael.lloyd@gmail.com"
	application.EnableBashCompletion = true

	application.Commands = []cli.Command{
		{
			Name:  "to-get",
			Usage: "any form of GET request, i.e: file downloads, viewing headers, etc.",
			Description: toGetHelp,
			Subcommands: []cli.Command{
				{
					Name:  "headers",
					Usage: "returns header information for a url.",

					Action: func(c *cli.Context) {
						headers(c.Args().First())
					},
				},
				{
					Name:  "file",
					Usage: "download a file.",
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "url, u",
							Usage: "url to download.",
						},
						cli.StringFlag{
							Name:  "file, f",
							Usage: "file to save download as.",
						},
					},
					Action: func(c *cli.Context) {
						downloader(c.String("url"), c.String("file"))
					},
				},
				{
					Name: "redirection",
					Usage: "see the final url in a redirect.",
					Action: func(c *cli.Context) {
						redirect(c.Args().First())
					},
				},
			},
			Action: func(c *cli.Context) {
				getter(c.Args().First())
			},
		},
	}

	application.Run(os.Args)
}

func getter(uri string) {
	resp, err := http.Get(uri)
	if err != nil {
		fmt.Errorf("error: ", err)
	}
	fmt.Print(resp, "\n")
}

func downloader(uri string, file string) {
	var log = logrus.New()
	log.Formatter = new(logrus.JSONFormatter)

	log.Debug("creating file.")
	outFile, err := os.Create(file)
	if err != nil {
		os.Exit(1)
	}

	client := &http.Client{}

	log.Debugf("new request for %+v", uri)
	req, err := http.NewRequest("GET", uri, nil)

	log.Debug("request initiated.")
	resp, err := client.Do(req)

	//progress bar
	header := resp.ContentLength
	bar := pb.New(int(header)).SetUnits(pb.U_BYTES)
	bar.Start()
	reader := bar.NewProxyReader(resp.Body)

	if err != nil {
		panic(err)
	}

	// not closing reader as the NewProxyReader exits for me.
	log.Debugf("copying response data to %+v", file)
	io.Copy(outFile, reader)

	os.Exit(0)
}

func headers(uri string) {
	var log = logrus.New()
	log.Formatter = new(logrus.JSONFormatter)

	log.Debug("reqesting headers.")
	resp, err := http.Head(uri)
	if err != nil {
		fmt.Errorf("error: ", err)
		log.Debug("error with url string: ", err)
		os.Exit(1)
	}

	log.Debug("reading headers.")
	for key, value := range resp.Header {
		log.Debugf("parsing %s for content.", key)
		fmt.Print(key, " ", value, "\n")
	}

	os.Exit(0)
}

func redirect(uri string) {
	resp, err := http.Get(uri)
	if err != nil {
		fmt.Errorf("error: ", err)
		os.Exit(1)
	}

	fmt.Print(resp.Request.URL.String(), "\n")
}
