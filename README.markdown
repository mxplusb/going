[![Build Status](https://travis-ci.org/mynameismevin/going.svg?branch=master)](https://travis-ci.org/mynameismevin/going)

# Going

This is a command-line client that currently does this:

* Downloads a file with a cool progress bar!
* Reads HTTP headers!
* Shows the final URL in a redirect!
* Uses a natural language feel as much as possible!

It will do this soon:

* Support all the HTTP methods.
* Upload files
* Profit

It's a work in progress!

### Going to Acquire

To acquire `going`, you have two options.

1. `go get github.com/mynameismevin/going` via the Go utility. This is preferred as you can just add the `-u` tag to update later.
2. Under releases, you can download the binary for your system. Once unzipped, make sure to rename it/symlink to `going`.

### Using

To use:

```bash
# GET request
going to-get httpAddr

# save a file
going to-get file -u httpFile -f fileName

# view headers
going to-get headers httpAddr

# view final url in redirection
going to-get redirection redirAddr
```

Please submit for issues, PRs, or ideas!
