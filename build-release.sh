#!/usr/bin/env bash

GOING_RELEASE="v0.1.3"
GOING_RELEASE_TYPE="rc.1"

if [ -d releases ]; then rm -rf releases/; fi

echo -e "clearing current builds."
if [ -e going ]
    then
    rm going;
fi

if [ -e going.exe ]
    then
    rm going.exe;
fi

echo -e "starting builds."
for GOOS in darwin windows linux freebsd openbsd netbsd; do
    for GOARCH in 386 amd64; do
        echo "Building $GOOS-$GOARCH"
        env GOOS=$GOOS GOARCH=$GOARCH go build -o releases/going-$GOOS-$GOARCH-$GOING_RELEASE-$GOING_RELEASE_TYPE going.go
        cd releases
        zip -9 going-$GOOS-$GOARCH-$GOING_RELEASE-$GOING_RELEASE_TYPE.zip going-$GOOS-$GOARCH-$GOING_RELEASE-$GOING_RELEASE_TYPE
        rm going-$GOOS-$GOARCH-$GOING_RELEASE-$GOING_RELEASE_TYPE
        cd ..
    done
done

unset GOING_RELEASE
unset GOING_RELEASE_TYPE

exit $?
